$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
        interval: 3000
    });
    $('.btn-reserva').click(function(e){
        console.log('clic en el btn-reserva');
        $(this).attr('id', 'id-btn-reserva');
        console.log($(this).prop('id'));
    });
    $('#reservaHotel').on('show.bs.modal', function (e){
        console.log('el modal reservaHotel se está mostrando');
        $('#id-btn-reserva').removeClass('btn-secondary');
        $('#id-btn-reserva').addClass('btn-info');
        $('.btn-reserva').attr('disabled', true);
    });
    $('#reservaHotel').on('shown.bs.modal', function (e){
        console.log('el modal reservaHotel se mostró');
    });
    $('#reservaHotel').on('hide.bs.modal', function (e){
        console.log('el modal reservaHotel se está ocultando');
    });
    $('#reservaHotel').on('hidden.bs.modal', function (e){
        console.log('el modal reservaHotel está oculto');
        $('#id-btn-reserva').removeClass('btn-info');
        $('#id-btn-reserva').addClass('btn-secondary');
        $('#id-btn-reserva').removeAttr('id');
        $('.btn-reserva').attr('disabled', false);
    });
});